﻿*******************************************************************************
** Component Information
*******************************************************************************

	Component:              ContentServerHeaderText
	Created By:             Redstone Content Solutions
	Updated By:             Jonathan Hult
	Last Updated On:        build_2_20130531

*******************************************************************************
** Overview
*******************************************************************************

	This component replaces the standard "Content Server" text with whatever 
	value is in the ContentServerHeaderText_Label preference prompt.
	
	Preference Prompts:
	ContentServerHeaderText_Label - Header text
	
*******************************************************************************
** COMPATIBILITY WARNING
*******************************************************************************

	This component was built upon and tested on the version listed below, 
	this is the only version it is "known" to work on, but it may well work 
	on older/newer versions.
	- 10.1.3.5.1 (111229) (Build: 7.2.4.105)

*******************************************************************************
** HISTORY
*******************************************************************************

	build_2_20130531
		- Updated for 11g compatibility
	build_1_20100506
		- Initial component release
